*** Settings ***
Suite Setup       Open Browser To Homepage
#Suite Teardown    Close All Browsers

Resource         ../resources/resource_profit.robot
Resource         ../resources/variables_profit.robot

*** Test Cases ***
Customer should be able to successfully login
    Open Login Page
    Complete With Valid Dates And Submit User Login

Customer should be able to select truck order option
    Open Order Type Page
    Select Trunck Order

Customer should be able to add a product to cart from the accessories category
    Open the Accessories page
    Select A Product From Accessories Page
    Add To Cart Button Selection From The Accessories Category

Customer should be able to add two product to cart from the details page
    Open The Couplings And Fittings Page
    Select A Product From The Couplings And Fittings Page
    Add To Cart Button Selection From The Couplings And Fittings Category

Customer should be able to search a product and add to cart
    Select Name Of The Product And Search Results
    Add To Cart Button Selection From The Couplings And Fittings Category
    Select A Variation And Search It Use Autocomplete
    Add To Cart Button Selection From The Couplings And Fittings Category

Customer should be able to go the cart page and check the products
    Go To Cart
    Check The Price For Products
    Submit Button From Cart

Customer should be able to select an Address and the Shipping Method
    Submit Current Addresses
    Select Shipping Method

Customer should be able to submit the Review order
    Submit Review Order


#   robot -d results -v ENVIRONMENT:stage -v BROWSER:chrome tests\01_truck_order.robot