*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported Selenium2Library.
Library             Selenium2Library

*** Keywords ***
Open Browser To Homepage
    Open Browser                          ${HOMEPAGE_URL}   ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed                    ${DELAY}

    Wait Until Element Is Visible         css=section[id="content"]   timeout=15

    Close Cookie

Open Login Page
    Header Isn't Visible

    Wait Until Element Is Visible         ${login_button}   timeout=15
    Click Element                         ${login_button}

    Wait Until Element Is Visible         css=section[class="notlogged"]   timeout=15

    Location Should Be                    ${LOGIN_URL}

Header Isn't Visible
    ${do_not_visible_header}              ${value}=       run keyword and ignore error   Element Should Be Visible       css=header[id="header"]
    Run Keyword If	                      '${do_not_visible_header}' == 'PASS'    Reload Page

Complete With Valid Dates And Submit User Login
    Insert Valid Email
    Insert Valid Password
    Submit User Login And Check Location Page

Insert Valid Email
    Wait Until Element Is Visible         css=input[name="email"]
    Input Text                            email           ${VALID_USER}

Insert Valid Password
    Wait Until Element Is Visible         css=input[name="password"]
    Input Text                            password        ${VALID_PASSWORD}

Submit User Login And Check Location Page
    Click Button                          css=button[class="button button--primary"]

    Wait Until Element Is Visible         xpath=//*[@id="content"]/section[1]/div/div[1]    timeout=15

Scroll Page To Location
    [Arguments]                           ${x_location}    ${y_location}
    Execute JavaScript                    window.scroll(${x_location},${y_location})

Close Cookie
    Wait Until Element Is Visible         css=div[id="gdpr_cookie_popup"]    timeout=15
    Click Element                         css=a[class="accept_gdrp btn btn-primary add-to-cart"]

    Wait Until Element Is Not Visible     css=div[id="gdpr_cookie_popup"]     timeout=15

Open Order Type Page
    Header Isn't Visible

    Wait Until Element Is Visible         ${icon_login}    timeout=15
    Click Element                         ${icon_login}

    Wait Until Element Is Visible         css=ul[aria-labelledby="accountLinks"] :nth-child(2)    timeout=15
    Click Element                         css=ul[aria-labelledby="accountLinks"] :nth-child(2)

    Location Should Be                    ${ORDER_TYPE_PAGE}

Select Trunck Order
    Wait Until Element Is Visible         ${truck_order}    timeout=15
    Click Element                         ${truck_order}

    Wait Until Element Is Visible         css=input[id="save_order_type"]    timeout=15
    Click Element                         css=input[id="save_order_type"]

    Button Reset Cart

    Wait Until Element Is Visible         css=article[class="alert alert-success"]    timeout=15

Select Container Order
    Wait Until Element Is Visible         ${container_order}    timeout=15
    Click Element                         ${container_order}

    Wait Until Element Is Visible         css=input[id="save_order_type"]    timeout=15
    Click Element                         css=input[id="save_order_type"]

    Button Reset Cart

    Wait Until Element Is Visible         css=article[class="alert alert-success"]    timeout=15

Button Reset Cart
    ${button_reset_cart}                ${value}=       run keyword and ignore error   Wait Until Element Is Visible       css=button[class="button button--primary "]       timeout=15
    Run Keyword If	                      '${button_reset_cart}' == 'PASS'    Click Element        css=button[class="button button--primary "]

Close Cart Reset Popup
    Click Element                         ${cart_reset_popup}

     Wait Until Element Is Not Visible    ${cart_reset_popup}    timeout=15

Add To Cart Button Selection From The Accessories Category
    Wait Until Element Is Visible         ${acccessories_button_add_to_cart}        timeout=15
    Click Element                         ${acccessories_button_add_to_cart}

    Wait Until Element Is Visible         css=div[class="alert alert-success"]       timeout=15

Add To Cart Button Selection From The Couplings And Fittings Category
    sleep                                 2s
    Wait Until Element Is Visible         ${second_button_add_to_cart}        timeout=15
    Click Element                         ${second_button_add_to_cart}

    Wait Until Element Is Visible         css=div[class="alert alert-success"]    timeout=15

Open the Accessories page
    Header Isn't Visible

    Wait Until Element Is Visible         ${accessories_category}    timeout=15

    Click Element                         ${accessories_category}
    Wait Until Element Is Visible         css=div[id="content-wrapper"]        timeout=15

    Location Should Be                    ${ACCESSORIES_PAGE}

Select A Product From Accessories Page
    Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(2)     timeout=15
    Click Element                         css=#js-product-list > div > article:nth-child(2)

    Wait Until Element Is Visible         css=div[class="product-information"]    timeout=15

Open The Couplings And Fittings Page
    ${button_isn't_visible}                ${value}=       run keyword and ignore error   Wait Until Element Is Visible       ${couplings_and_fittings}       timeout=15
    Run Keyword If	                      '${button_isn't_visible}' == 'PASS'    Reload Page

    Click Element                         ${couplings_and_fittings}

    Wait Until Element Is Visible         css=section[id="products"]     timeout=15

Select A Product From The Couplings And Fittings Page
    Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(6)    timeout=15
    Click Element                         css=#js-product-list > div > article:nth-child(6)

    Wait Until Element Is Visible         css=div[class="product-information"]    timeout=15

    Add The Quantity For Products

Add The Quantity For Products
    Select Quantity For First Product From Tabel
    Select Quantity For A Product From Accessories Block

Select Quantity For First Product From Tabel
    scroll page to location               0   500

    First Address

    Second Address

First Address
    ${address_1}                          ${value}=       run keyword and ignore error   Wait Until Element Is Visible       xpath=//*[@id="quantity_truck_2568"]        timeout=15
    Run Keyword If	                      '${address_1}' == 'PASS'    Input Text                            xpath=//*[@id="quantity_truck_2568"]      20

Second Address
    ${address_2}                          ${value}=       run keyword and ignore error   Wait Until Element Is Visible       xpath=//*[@id="quantity_truck_798"]        timeout=15
    Run Keyword If	                      '${address_2}' == 'PASS'    Input Text                            xpath=//*[@id="quantity_truck_798"]      20

Select Quantity For A Product From Accessories Block
    scroll page to location               0   1000

    Wait Until Element Is Visible         xpath=//*[@id="main"]/section/section/div/div/div[1]/article/div/div[2]/div[3]/div[1]/input    timeout=15

    Input Text                            xpath=//*[@id="main"]/section/section/div/div/div[1]/article/div/div[2]/div[3]/div[1]/input     5

Select Firs Product From Browse
    Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(2)    timeout=15
    Click Element                         css=#js-product-list > div > article:nth-child(2)

    Wait Until Element Is Visible         css=div[class="product-information"]    timeout=15

    Select Quantity

Select Quantity
    scroll page to location               0   500

    Wait Until Element Is Visible         xpath=//*[@id="main"]/div[6]/div/div/table/tbody/tr[3]/td[6]/div/div[2]/button   timeout=15
    Click Element                         xpath=//*[@id="main"]/div[6]/div/div/table/tbody/tr[3]/td[6]/div/div[2]/button

    Add Quantity

Add Quantity
    Popup Add Quantity

    Wait Until Element Is Visible         css=button[class="button button--primary"]    timeout=15
    Click Element                         css=button[class="button button--primary"]

    Wait Until Element Is Not Visible     css=div[class="fancybox-wrap fancybox-desktop fancybox-type-inline fancybox-opened"]    timeout=15

Popup Add Quantity
    Wait Until Element Is Visible         css=input[id="container-editor-boxes"]    timeout=15
    Input Text                            css=input[id="container-editor-boxes"]    11

Select Name Of The Product And Search Results
    Open The Couplings And Fittings Page

#Open details page about second product
    Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(4)    timeout=15
    Click Element                         css=#js-product-list > div > article:nth-child(4)

#save the name of the product
    Wait Until Element Is Visible         css=h1[itemprop="name"]    timeout=15

    ${PRODUCT_NAME}=                      Get Text    css=h1[itemprop="name"]

#Use the search field
    Wait Until Element Is Visible         ${search_field}    timeout=15
    Input Text                            ${search_field}     ${PRODUCT_NAME}

    Wait Until Element Is Visible         ${search_button}    timeout=15
    Click Element                         ${search_button}

    ${product_searched_1}                 ${value}=       run keyword and ignore error   Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(30)    timeout=15
    Run Keyword If	                      '${product_searched_1}' == 'PASS'    Open Details Page For The Product And Add A Quantity (1)

    ${product_searched_2}                 ${value}=       run keyword and ignore error   Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(24)    timeout=15
    Run Keyword If	                      '${product_searched_2}' == 'PASS'    Open Details Page For The Product And Add A Quantity (2)

Open Details Page For The Product And Add A Quantity (1)
    Wait Until Element Is Visible         css=section[id="products"]    timeout=15

    scroll page to location               0   1000

    Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(30)    timeout=15
    Click Element                         css=#js-product-list > div > article:nth-child(30)

    Wait Until Element Is Visible         css=h1[itemprop="name"]    timeout=15

    Select Quantity For First Element From Tabel

Select Quantity For First Element From Tabel
    scroll page to location               0   500

    First Element

    Second Element

    Click Element                         css=tr[class="variations-table-heading"]

First Element
    ${first_element_visible}              ${value}=       run keyword and ignore error   Wait Until Element Is Visible       xpath=//*[@id="quantity_truck_403"]        timeout=15
    Run Keyword If	                      '${first_element_visible}' == 'PASS'    Input Text                            xpath=//*[@id="quantity_truck_403"]      5

Second Element
    ${second_element_visible}             ${value}=       run keyword and ignore error   Wait Until Element Is Visible       xpath=//*[@id="quantity_truck_2644"]        timeout=15
    Run Keyword If	                      '${second_element_visible}' == 'PASS'    Input Text                            xpath=//*[@id="quantity_truck_2644"]      5

Open Details Page For The Product And Add A Quantity (2)
    Wait Until Element Is Visible         css=section[id="products"]    timeout=15

    scroll page to location               0   1000

    Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(24)    timeout=15
    Click Element                         css=#js-product-list > div > article:nth-child(24)

    Wait Until Element Is Visible         css=h1[itemprop="name"]    timeout=15

    Select Quantity

Select A Variation And Search It Use Autocomplete
    Open The Couplings And Fittings Page

    Wait Until Element Is Visible         css=#js-product-list > div > article:nth-child(8)    timeout=15
    Click Element                         css=#js-product-list > div > article:nth-child(8)

#save the name of the product
    Wait Until Element Is Visible         css=span[id="GSTFCR11/4-1/2"]    timeout=15

    ${PRODUCT_NAME}=                      Get Text    css=span[id="GSTFCR11/4-1/2"]

#Use the search field
    Wait Until Element Is Visible         ${search_field}    timeout=15
    Input Text                            ${search_field}     ${PRODUCT_NAME}

    Wait Until Element Is Visible         css=ul[id="ui-id-1"]    timeout=15
    Click Element                         css=ul[id="ui-id-1"]

    Insert Quantity For Autocomplete Product

    Click Element                         css=tr[class="variations-table-heading"]
    sleep                                 2s

Insert Quantity For Autocomplete Product
    ${first_address_visible}             ${value}=       run keyword and ignore error   Wait Until Element Is Visible       xpath=//*[@id="quantity_truck_790"]        timeout=15
    Run Keyword If	                      '${first_address_visible}' == 'PASS'    Input Text                    xpath=//*[@id="quantity_truck_790"]      10

    ${second_address_visible}             ${value}=       run keyword and ignore error   Wait Until Element Is Visible       xpath=//*[@id="main"]/div[6]/div/div/table/tbody/tr[3]/td[6]/div/div[2]/button        timeout=15
    Run Keyword If	                      '${second_address_visible}' == 'PASS'    Select Quantity

Go To Cart
    Wait Until Element Is Visible         ${cart_dropdown}        timeout=15
    Click Element                         ${cart_dropdown}

    Wait Until Element Is Visible         ${view_cart}        timeout=15
    Click Element                         ${view_cart}

    Location Should Be                    ${CART_PAGE}

Go To Cart Container
    Wait Until Element Is Visible         ${cart_dropdown_container}        timeout=15
    Click Element                         ${cart_dropdown_container}

    Location Should Be                    ${CART_PAGE}

Submit Button From Cart
    scroll page to location               0   1000

    Wait Until Element Is Visible         ${submit_button_from_cart}   timeout=15
    Click Element                         ${submit_button_from_cart}

    Wait Until Element Is Visible         css=section[id="checkout-addresses-step"]    timeout=15

    Location Should Be                    ${ORDER_PAGE}

Submit Current Addresses
    Wait Until Element Is Visible         ${submit_address}   timeout=15
    Click Element                         ${submit_address}

    Wait Until Element Is Visible         css=div[class="form-fields"]      timeout=15

Submit Current Addresses Container
    Wait Until Element Is Visible         ${submit_address_container}   timeout=15
    Click Element                         ${submit_address_container}

    Wait Until Element Is Visible         css=div[class="condition-label"]      timeout=15

Check The Price For Container's Products
    ${get_price1}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[3]/div/div[1]/div[2]/table/tbody/tr/td[9]
    ${get_price2}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[3]/div/div[2]/div[2]/table/tbody/tr/td[9]
    ${get_price3}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[3]/div/div[3]/div[2]/table/tbody/tr/td[9]

# Product price is compared with zero value:
     Should Not Be Equal                  ${get_price1}   ${get_price0}
     Should Not Be Equal                  ${get_price2}   ${get_price0}
     Should Not Be Equal                  ${get_price3}   ${get_price0}

Check The Price For Products
    ${get_price1}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[3]/div/div[1]/div[2]/table/tbody/tr/td[10]
    ${get_price2}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[3]/div/div[2]/div[2]/table/tbody/tr/td[10]
    ${get_price3}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[3]/div/div[3]/div[2]/table/tbody/tr/td[10]
    ${get_price4}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[4]/div/div[2]/table/tbody/tr/td[10]
    ${get_price5}=                        Get Text    xpath=//*[@id="main"]/div[2]/div/div[5]/div/div[2]/table/tbody/tr/td[10]

# Product price is compared with zero value:
     Should Not Be Equal                  ${get_price1}   ${get_price0}
     Should Not Be Equal                  ${get_price2}   ${get_price0}
     Should Not Be Equal                  ${get_price3}   ${get_price0}
     Should Not Be Equal                  ${get_price4}   ${get_price0}
     Should Not Be Equal                  ${get_price5}   ${get_price0}

Select Shipping Method
    ${shipping_method_selected}           ${value}=       run keyword and ignore error   Checkbox Should Be Selected        ${submit_shipping_method}
    Run Keyword If	                      '${shipping_method_selected}' == 'PASS'    Select Shipping

Select Shipping
    Wait Until Element Is Visible         ${submit_shipping_method}         timeout=15
    Click Element                         ${submit_shipping_method}

Submit Review Order
# Marius am nevoie de acest slip ca sa imi bifeze caseta de la review order cant customer-ul e logat cu truck order
    sleep                   2s
    scroll page to location               0   1000

    Wait Until Element Is Visible         ${submit_review_order}           timeout=15
    Click Element                         ${submit_review_order}







