*** Variables ***
${BROWSER}               ${ROBOT_BROWSER}

&{SERVER}               stage=http://profit:profit@staging.profittings.eu/en/

${VALID_USER}          bdimitrov@chrysmetal.bg
${VALID_PASSWORD}      123456

${LOGIN_URL}            http://staging.profittings.eu/en/login?back=my-account

${HOMEPAGE_URL}         ${SERVER.${ENVIRONMENT}}

${ORDER_TYPE_PAGE}      http://staging.profittings.eu/en/module/profittings/customer

${PAYPLAZA_PAGE}        https://pay.mollie.nl/payment/start/c2f09fda09089f9a73e62ac1c38fbe79

${CART_PAGE}            http://staging.profittings.eu/en/cart?action=show

${ORDER_PAGE}           http://staging.profittings.eu/en/order?use_same_address=1

${ACCESSORIES_PAGE}     http://staging.profittings.eu/en/30-accessories

${CUSTOMIZE_PAGE}       http://staging.profittings.eu/module/coincustomization/customcoin?id_product=29260

#  USER _________________________________________________________
${DELAY}                0

# Submit Button From Cart
${submit_button_from_cart}    css=a[class="button button--primary full large MB20"]

# Submit address button
${submit_address}             xpath=//*[@id="id-address-delivery-address-8401"]/header/label/span
${submit_address_container}   xpath=//*[@id="id-address-delivery-address-8064"]/header/label/span

#Select Shipping Method
${submit_shipping_method}     css=span[class="custom-radio pull-xs-left"]

# Submit Review Order
${submit_review_order}        xpath=//*[@id="wrapper"]/div/ul/li/div[1]/span

#Details Page
${cart_dropdown}              css=a[class="button button--header-main hidden-sm-down"]
${cart_dropdown_container}    css=div[id="_desktop_cart"]
${view_cart}                  xpath=//*[@id="_desktop_cart"]/div/div/div/div/div[2]/div[1]
${acccessories_button_add_to_cart}         css=button[data-button-action="add-to-cart"]
${second_button_add_to_cart}  css=button[onclick="addProductToCart()"]

${get_price0}                 €0.00

# Header
${icon_login}                 css=div[class="user-info dropdown"]
${login_button}               css=a[class="button button--header-main"]
${search_field}               css=input[placeholder="Search for a product"]
${search_button}              xpath=//*[@id="search_widget"]/form/button/i
${accessories_category}       css=li[id="category-30"]
${couplings_and_fittings}     css=li[id="category-27"]

#Account
${truck_order}                xpath=//*[@id="order-type-selector"]/div[1]/label/span
${container_order}            xpath=//*[@id="order-type-selector"]/div[3]/label/span


